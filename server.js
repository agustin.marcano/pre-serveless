import app from "./src/app";
import setting from "./src/config/setting";

const port = setting.PORT;

app.listen(port, () => {
  console.log("You have successfully connected to port " + port);
});
