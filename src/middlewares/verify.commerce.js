import User from "../models/User";


export const verifyCommerce = async (req, res, next) => {
  const userIdLoggedIn = req.userId;
  const commerceIdParam = req.params.id;

  const user = await User.findById(userIdLoggedIn);

  if (user.commerce){
    if (user.commerce.equals(commerceIdParam) == false)
      return res.status(403).json({ message: "Forbidden" });
  } else {
    return res.status(403).json({ message: "Forbidden" });
  }

  next();
};
