export const verifyUser = async (req, res, next) => {

  const userIdLoggedIn = req.userId;
  const userIdParam = req.params.id;

  if (userIdLoggedIn != userIdParam)
    return res.status(403).json({ message: "Forbidden" });

  next();
}