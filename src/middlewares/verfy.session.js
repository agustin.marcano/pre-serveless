import jwt from "jsonwebtoken";
import setting from "../config/setting";
import User from "../models/User";

export const verifyToken = async (req, res, next) => {
  let access = req.headers["authorization"];
  const [, token] = access?.split(" ") ?? [null, null];

  if (req.headers["authorization"] == undefined)
    return res.status(500).json({ message: "No Header provided" });
  else if (!token)
    return res.status(403).json({ message: "No token provided" });

  try {
    const decoded = jwt.verify(token, setting.SECRET);
    req.userId = decoded.id;

    const user = await User.findById(req.userId, { password: 0 });
    if (!user) return res.status(404).json({ message: "No user found" });

    next();
  } catch (error) {
    console.log(error);
    return res.status(401).json({ message: "Unauthorized!" });
  }
};
