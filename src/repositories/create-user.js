import { messageError } from "../helpers/catch.errors";
import User from "../models/User";
import Commerce from "../models/Commerce";

export const createUser = async (params) => {

  try {
    const { name, username, email, password, commerce_id } = params;
    let data = {
      name,
      username,
      email,
      password: await User.encryptPassword(password),
    };

    if (commerce_id != undefined) {
      const commerce = await Commerce.findById(commerce_id);

      if (commerce) {
        data = { ...data, commerce };
      }
    }

    const newUser = new User(data);

    const response = await newUser.save();

    return { success: true, data: response };

  } catch (err) {
    const message = messageError(err);

    res.status(500).json(message);
  }
};