import mongoose from "mongoose";
import setting from "./setting";

const URL = setting.MONGODB_URI;
const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  // useCreateIndex: true,
  // useFindAndModify: true
};

mongoose
  .connect(URL, options)
  .then((db) => console.log("DB is connected"))
  .catch((error) => console.log(error));
