import { config } from "dotenv";
config();

export default {
  MONGODB_URI:
    process.env.MONGODB_URI || "mongodb://localhost/api-preserveless",
  PORT: process.env.PORT || 4000,
  SECRET: "api-preserveless",
  URL_PAYMENT_WEBHOOK: "https://envj4s52ox47.x.pipedream.net",
};
