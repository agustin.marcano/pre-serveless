const { Router } = require("express");
const router = Router();

import { body } from "express-validator";

import * as authController from "../controllers/auth.controller";
import { fieldsValidation } from "../middlewares/resultValidated";

router.post(
  "/signup",
  [
    body("username", "Username is required").notEmpty(),
    body("email", "Email is not a valid address").isEmail(),
    body("name", "Name is required").notEmpty(),
    body("password")
      .notEmpty()
      .withMessage("Password is required")
      .isLength({ min: 8 })
      .withMessage("must be at least 8 chars long"),
    fieldsValidation,
  ],
  authController.signUp
);
router.post("/signin", authController.signIn);

export default router;