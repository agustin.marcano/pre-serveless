import { Router } from "express";
import { body, check } from "express-validator";

import * as commerceController from "../controllers/commerce.controllers";
import { fieldsValidation } from "../middlewares/resultValidated";
import { verifyToken } from "../middlewares/verfy.session";
import { verifyCommerce } from "../middlewares/verify.commerce";

const router = Router();

router.get("/", commerceController.getAll);
router.post(
  "/",
  [
    verifyToken,
    body("name", "Name is required").notEmpty(),
    body("street", "Street is required").notEmpty(),
    body("phone", "Phone is required").notEmpty(),
    body("email")
      .optional()
      .isEmail()
      .withMessage("Email is not a valid address"),
    fieldsValidation,
  ],
  commerceController.newRecord
);
router.get("/detail/:id", commerceController.getById);
router.put("/:id", [verifyToken, verifyCommerce], commerceController.updateById);
router.delete("/:id", [verifyToken, verifyCommerce], commerceController.deleteById);


export default router;
