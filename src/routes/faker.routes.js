import { Router } from "express";
import {
  generatePaymentRequest,
  generateCreditCard,
} from "../controllers/faker.controller";
const router = Router();

router.post("/generate-payment-request", generatePaymentRequest);
router.post("/generate-credit-card", generateCreditCard);

export default router;