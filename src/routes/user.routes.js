import { Router } from "express";
import { body, check } from "express-validator";

import * as userController from "../controllers/users.controller";
import { fieldsValidation } from "../middlewares/resultValidated";
import { verifyToken } from "../middlewares/verfy.session";
import { verifyUser } from "../middlewares/verify.user";
;
const router = Router();

router.get("/", userController.getUsers);
router.post(
  "/",
  [
    verifyToken,
    body("username", "Username is required").notEmpty(),
    body("email", "Email is not a valid address").isEmail(),
    body("name", "Name is required").notEmpty(),
    body("password")
      .notEmpty()
      .withMessage("Password is required")
      .isLength({ min: 8 })
      .withMessage("must be at least 8 chars long"),
    body("commerce_id", "Commerce ID is required").notEmpty(),
    fieldsValidation,
  ],
  userController.newRecord
);
router.get(
  "/detail/:id",
  [check("id", "ID is required").notEmpty()],
  userController.getUserById
);
router.put(
  "/:id",
  [
    verifyToken,
    verifyUser,
    // oneOf([
    //   body("username").notEmpty().withMessage("Username is required"),
    //   body("email").not().isEmail().withMessage("Email is not a valid address"),
    //   body("name").notEmpty().withMessage("Name is required"),
    //   body("password")
    //     .notEmpty()
    //     .withMessage("Password is required")
    //     .isLength({ min: 8 })
    //     .withMessage("must be at least 8 chars long"),
    // ]),
    // fieldsValidation,
  ],
  userController.updateUserById
);
router.delete("/:id", [verifyToken, verifyUser], userController.deleteUserById);

export default router;
