import { Router } from "express";
import { checkout } from "../controllers/payment.controller";
import { verifyToken } from "../middlewares/verfy.session";

const router = Router();

router.post("/checkout", [verifyToken], checkout);

export default router;