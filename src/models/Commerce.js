import { Schema, model } from "mongoose";

const Commerce = new Schema(
  {
    name: {
      type: String,
      unique: true,
      required: true,
      trim: true,
    },
    phone: {
      type: String,
      required: true,
      trim: true,
    },
    street: {
      type: String,
      required: true,
      trim: true,
    },
    email: String,
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

export default model("Commerce", Commerce);
