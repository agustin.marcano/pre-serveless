import { Schema, model } from "mongoose";

const Payment = new Schema(
  {
    payment_date: {
      type: Date,
      default: Date.now(),
    },
    total: {
      type: Number,
      required: true,
    },
    creditCard: {
      type: Schema.Types.ObjectId,
    }
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

export default model("payment", Payment);