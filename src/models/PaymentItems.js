import { Schema, model } from "mongoose";

const PaymentItem = new Schema(
  {
    product_name: {
      type: String,
      required: true,
    },
    qty: {
      type: Number,
      required: true,
    },
    price_unit: {
      type: Number,
      required: true,
    },
    payment_request: {
      type: Schema.Types.ObjectId,
      required: true,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

export default model("payment_item", PaymentItem);
