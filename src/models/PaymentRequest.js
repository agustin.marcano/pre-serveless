import { Schema, model } from "mongoose";

const PaymentRequest = new Schema(
  {
    total: {
      type: Number,
      // required: true,
      default: 0
    },
    payment: {
      type: Schema.Types.ObjectId,
      // required: false,
      default: null,
    },
    status: {
      type: String,
      default: 'Pending'
    },
    intents: {
      type: Number,
      default: 0,
      max: 3
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

export default model("payment_request", PaymentRequest);