import bcrypt from "bcryptjs";
import { Schema, model } from "mongoose";

const User = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    username: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    commerce: {
      type: Schema.Types.ObjectId,
      ref: "Commerce",
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

User.statics.encryptPassword = async (password) => {
  const salt = await bcrypt.genSalt(10);

  return await bcrypt.hash(password, salt);
};

User.statics.comparePassword = async (password, receivedPassword) => {
  const compare = await bcrypt.compare(password, receivedPassword);

  return compare;
};

export default model("User", User);
