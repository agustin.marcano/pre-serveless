import { Schema, model } from "mongoose";

const CreditCard = new Schema(
  {
    number: {
      type: String,
      required: true,
    },
    cvv: {
      type: Number,
      required: true,
    },
    status: {
      type: Boolean,
      required: true,
    },
    balance: {
      type: Number,
      default: 0,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

export default model("credit_card", CreditCard);