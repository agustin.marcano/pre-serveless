const express = require("express");
const morgan = require("morgan");
const app = express();

import "./config/database";

import authRoutes from "./routes/auth.routes";
import commerceRoutes from "./routes/commerce.routes";
import fakerRoutes from "./routes/faker.routes";
import paymentRoutes from "./routes/payment.routes";
import userRoutes from "./routes/user.routes";

app.use(morgan("dev"));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use("/api/v1/auth", authRoutes);
app.use("/api/v1/commerce", commerceRoutes);
app.use("/api/v1/faker", fakerRoutes);
app.use("/api/v1/payment", paymentRoutes);
app.use("/api/v1/users", userRoutes);

export default app;