import faker from "faker";
import PaymentRequest from "../models/PaymentRequest";
import PaymentItems from "../models/PaymentItems";
import CreditCard from "../models/CreditCard";

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

export const generatePaymentRequest = async (req, res) => {
  try {
    const N_REQUEST = getRandomInt(1, 10);
    let response = [];

    for (let tmp = 0; tmp < N_REQUEST; tmp++) {
      const paymentRequest = new PaymentRequest();
      const PR = await paymentRequest.save();
      let total_amount = 0;
      const N_ITEM = getRandomInt(1,10);

      for (let row = 0; row < N_ITEM; row++) {
        const price = faker.commerce.price();
        total_amount += Number.parseFloat(price);

        const paymentItems = new PaymentItems({
          product_name: faker.commerce.productName(),
          qty: 1,
          price_unit: price,
          payment_request: PR,
        });

        await paymentItems.save();
      }

      const data = await PaymentRequest.findByIdAndUpdate(
        { _id: PR._id },
        { total: total_amount },
        { new: true }
      );

      response = [...response, data];
    }

    res.json(response);
  } catch (error) {
    console.log(error);
  }
};

export const generateCreditCard = async (req, res) => {
  const N_REQUEST = getRandomInt(1, 10);
  let response = [];

  for ( let row = 0; row < N_REQUEST; row++ ) {
    const creditCard = new CreditCard({
      number: faker.finance.creditCardNumber(),
      cvv: faker.finance.creditCardCVV(),
      status: getRandomInt(0, 2) == 0 ? false : true,
      balance: faker.finance.amount(-500, 5000)
    });

    const data = await creditCard.save();

    response = [...response, data];
  }

  res.json(response);
}