import jwt from "jsonwebtoken";
import config from "../config/setting";
import { createUser } from "../repositories/create-user";
import User from "../models/User";

export const signUp = async (req, res) => {
  try {
    const body = req.body;
    // const { name, username, email, password } = body;

    const response = await createUser(body);

    if (response.success == true) {
      const token = jwt.sign({ id: response._id }, config.SECRET, {
        expiresIn: 86400, // 24 hours
      });

      res.json({ ...response, token });
    } else {
      throw response;
    }

  } catch (error) {
    return res.status(500).json(error);
  }
};

export const signIn = async (req, res) => {
  try {

    const { email, password } = req.body;

    const userFound = await User.findOne({ email: email });

    if (!userFound) return res.status(400).json({ message: "User Not Found" });

    const matchPassword = await User.comparePassword(
      password,
      userFound.password
    );

    if (!matchPassword)
      return res.status(401).json({
        token: null,
        message: "Invalid Password",
      });

    const token = jwt.sign({ id: userFound._id }, config.SECRET, {
      expiresIn: 86400, // 24 hours
    });

    res.json({ token });
  } catch (error) {
    console.log(error);
  }
};
