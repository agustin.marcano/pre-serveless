import { messageError } from "../helpers/catch.errors";
import Commerce from "../models/Commerce";

export const newRecord = async (req, res) => {
  const { name, phone, street, email } = req.body;

  try {
    const newCommerce = new Commerce({
      name,
      phone,
      street,
      email,
    });

    const response = await newCommerce.save();

    res.json(response);

  } catch (error) {
    const message = messageError(error);

    res.status(500).json(message);
  }

}

export const getAll = async (req, res) => {
  const data = await Commerce.find() ?? [];
  res.json(data);
}

export const getById = async (req, res) => {
  const id = req.params.id;
  const data = await Commerce.findById(id) ?? [];

  res.json(data);
}

export const updateById = (req, res) => {
  const id = req.params.id;
  const body = req.body;

  Commerce.findByIdAndUpdate({ _id: id }, body, { new: true }, (err, user) => {
    if (err) {
      const message = messageError(err);

      res.status(500).json(message);
    }

    res.json({ message: "Has been processed successfully" });
  });
}

export const deleteById = (req, res) => {
  const id = req.params.id;

  Commerce.deleteOne({ _id: id }, (err, user) => {
    if (err) res.status(500).send(err);

    res.json({ message: "Has been processed successfully" });
  });
}