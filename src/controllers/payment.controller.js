import PaymentRequest from "../models/PaymentRequest"
import CreditCard from "../models/CreditCard";
import Payment from "../models/Payment";
import axios from "axios";

import setting from "../config/setting";

const paymentRequestUpdate = async (id, data) => {
  return PaymentRequest.findByIdAndUpdate({ _id: id }, data, { new: true });
}

const webhookNotification = async (message) => {
  await axios.post(setting.URL_PAYMENT_WEBHOOK, { message });
}

const updateCreditCard = async (id, creditCardAmount, paymentAmount) => {
  return await CreditCard.findOneAndUpdate(
    { _id: id },
    {
      balance:
        Number.parseFloat(creditCardAmount) - Number.parseFloat(paymentAmount),
    }
  );
}

export const checkout = async (req, res) => {
  const card_id = req.body.card_id ?? null;
  const card_number = req.body.number ?? null;
  const request_id = req.body.request_id ?? null;

  if (card_id !== null && request_id !== null) {
    let paymentRequest = await PaymentRequest.findById(request_id);
    let creditCard = [];
    let message = "";

    if (paymentRequest.intents == 3) {
      return res.status(423).json({
        message: "Payment request blocked",
      });
    } else {
      if (card_id) {
        creditCard = await CreditCard.findById(card_id);
      } else {
        creditCard = await CreditCard.findOne({ number: card_number });
      }

      if (
        creditCard.balance < 0 ||
        creditCard.status == false ||
        paymentRequest.total > creditCard.balance
      ) {
        const data = {
          intents: paymentRequest.intents + 1,
          status: paymentRequest.intents + 1 == 3 ? "Blocked" : "Pending",
        };

        message = "Credit Card inactive o insufficient balance";

        await webhookNotification(message);
        await paymentRequestUpdate(request_id, data);

        return res
          .status(409)
          .json({ message });
      }

      if (paymentRequest.total <= creditCard.balance) {
        let payment = new Payment({
          total: paymentRequest.total,
          creditCard,
        });

        payment = await payment.save();

        const data = {
          total: paymentRequest.total,
          payment,
          status: "Approved",
          intents: paymentRequest.intents + 1,
        };

        message = "Payment completed";

        await paymentRequestUpdate(request_id, data);
        await updateCreditCard(creditCard._id, creditCard.balance, paymentRequest.total);
        await webhookNotification(message);

        return res.json({ message });
      } else {
        const data = {
          intents: paymentRequest.intents + 1,
          status: paymentRequest.intents + 1 == 3 ? "Blocked" : "Pending",
        };

        await webhookNotification(message);
        await paymentRequestUpdate(request_id, data);
      }
    }
  }

  res.status(404).json({
    message: "Error, parameters invalid"
  });

};
