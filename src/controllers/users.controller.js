import User from "../models/User";
import { createUser } from "../repositories/create-user";

export const newRecord = async (req, res) => {
  try {
    const body = req.body;

    const response = await createUser({ ...body });

    if (response.success == true) {
      res.json(response.data);
    } else {
      throw response;
    }
  } catch (error) {
    return res.status(500).json(error);
  }
};

export const getUsers = async (req, res) => {

  const data = (await User.find().populate("commerce")) ?? [];
  res.json(data);

};

export const getUserById = async (req, res) => {
  const id = req.params.id;
  const data = (await User.findById(id)) ?? [];

  res.json(data);
};

export const updateUserById = async (req, res) => {
  const id = req.params.id;
  const body = req.body;

  User.findByIdAndUpdate({ _id: id }, body, { new: true }, (err, user) => {
    if (err){
      const message = messageError(err);

      res.status(500).json(message);
    }

    res.json({ message: "Has been processed successfully" });
  })
};

export const deleteUserById = (req, res) => {
  const id = req.params.id;

  User.deleteOne({ _id: id }, (err, user) => {
    if (err) res.status(500).send(err);

    res.json({ message: "Has been processed successfully" });
  });
};