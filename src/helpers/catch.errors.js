export const messageError = (err) => {
  if (err.hasOwnProperty("errors"))
    return { success: false, type: err.name, message: err.message };
  else {
    const valueDuplicated = Object.values(err.keyValue)[0];

    return {
      success: false,
      type: "valueDuplicated",
      message: `The value ${valueDuplicated} already exist.`,
    };
  }
}